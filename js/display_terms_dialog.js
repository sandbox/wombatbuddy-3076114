/**
 * @file
 * Create Ajax command for display the terms confirmation dialog.
 */

(function ($) {

  Drupal.ajax.prototype.commands.display_terms_dialog = function(ajax, response, status) {

    $('body').append('<div id ="terms-dialog"></div>');
    $('#terms-dialog').html(response.text_of_terms);
    $('#terms-dialog').dialog({
      autoOpen: true,
      modal: true,
      title: 'Terms and conditions',
      buttons : [
        {
          id: 'Confirm',
          text: 'I agree',
          click : function() {
            // Click on the related hidden button of the 'Workflow' module.
            $('#' + response.workflow_hidden_button_id).click();
            $(this).dialog("close");
          }
        },
        {
          id: 'Cancel',
          text: 'I don\'t agree',
          click : function() {
            window.history.back();
            $(this).dialog("close");
          }
        }
      ]
    });
    }
})(jQuery);